/**
 * NoScript Tracker
 *  Get basic analytics without requiring Client-Side JavaScript
 *  
 *  See README.md for configuration & usage
 */


/** Load the configuration */
var config = require('./config');
log('Loaded configuration');

/** Dependencies */
var crypto = require('crypto');
var express = require('express');


/** Hash containing active visitors */
var visitors = {};


/** Create tracker functions */
var tracker = {};

// Handle new visitor requests
tracker.newVisitor = function(request, response) {
    
    // Create a vistor object
    var visitor = {
        ip: request.ip,
        timeout: null,
        seconds: 0,
    };
    
    // Generate a random token
    crypto.randomBytes(6, function(error, buffer) {
        
        // Encode the token in hexadecimal
        var token = buffer.toString('hex');
        
        // Add the visitor to the active list
        visitors[token] = visitor;
        
        // Log the new visitor
        log('%s (%s) is now visiting', token, request.ip);
        
        // Send the new ping delay
        response.status(302);
        response.set('Location', token);
        response.end();
        
    });
    
};

tracker.handlePing = function(request, response) {
    
    // Get the visitor information
    var token = request.params.token;
    var visitor = visitors[token];
    
    // Check for invalid visitors
    if (!visitor) {
        // Tell the user to get a new token
        log('%s (%s) sent an invalid token, refreshing', token, request.ip);
        return tracker.newVisitor(request, response);
    }
    
    // Send the new ping delay
    tracker.reply(token, response);
    
};

// Reply with refresh token
tracker.reply = function(token, response) {
    
    // Get the visitor information
    var visitor = visitors[token];
    
    // Clear the token timeout
    if (visitor.timeout) clearTimeout(visitor.timeout);
    
    // Update the seconds and get the delay
    visitor.seconds += config.delayFunction(visitor.seconds);
    var delay = config.delayFunction(visitor.seconds);
    
    // Send the new ping delay
    response.status(200);
    response.set('Refresh', delay);
    response.end();
    
    // Set the new timeout
    visitor.timeout = setTimeout(
        tracker.timeout.bind(null, token), 
        (config.delayFunction(visitor.seconds) + config.networkLeeway) * 1000
    );
    
};

// Handle visitors who have left the page
tracker.timeout = function(token) {
    
    // Get the visitor information
    var visitor = visitors[token];
    
    // Log information about their stay
    log(
        '%s (%s) visited around %d to %d seconds',
        token, visitor.ip,
        visitor.seconds,
        visitor.seconds + config.delayFunction(visitor.seconds)
    );
    
    // Remove the visitor from the active list
    delete visitors[token];
    
};


/** Create webserver */
var server = express();

// Strip headers
server.disable('x-powered-by');
server.use(function(request, response, next) {
    response.set('Content-Length', 0);
    next();
});

// Serve tracking URLs
server.get('/',       tracker.newVisitor);
server.get('/:token', tracker.handlePing);

// Start the server
server.listen(config.port, function(error) {
    
    // Check for errors
    error?
        log('Error occured while starting server: %s', error)
    :   log('Started server on :%d', config.port);
    
});


/** Pretty log function */
function log(message/*, values... */) {
    
    // Get the values
    var values = [].slice.call(arguments, 1);
    
    // Add a timestamp
    var timestamp = new Date().toTimeString().split(' ')[0];
    message = '[%s] ' + message;
    values = [timestamp].concat(values);
    
    // Log the message
    console.log.apply(console, [message].concat(values));
    
}
