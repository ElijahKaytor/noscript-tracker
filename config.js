/** Configuration */
var config = {
    
    // HTTP Server port
    port: 4001,
    
    // Tracking accuracy
    accuracy: 0.25,
    
    // Amount of time (in seconds) to account for network delays
    networkLeeway: 0.5,
    
    // Function to determine ping delay
    delayFunction: function(seconds) {
        
        if (seconds < 1)
            return 1;
        if (seconds < 8) 
            return Math.ceil(seconds * config.accuracy);
        else 
            return Math.floor(seconds * config.accuracy);
        
    },
    
};

// Export the config
module.exports = config;
