## NoScript Tracker
#### Get basic analytics without requiring Client-Side JavaScript


-----
### Description

NoScript Tracker is a basic tracker that makes use of ``iframes`` and the ``Refresh`` HTTP header to measure how long users spend on web pages.

It is ideal for getting basic usage statistics on the Tor network, where JavaScript is not an option for most users.


-----
### Installation

Requires [Node.js](http://nodejs.org/)

    $ npm install
      [truncated output]
      
      Done, without errors.


-----
### Usage

Start the server with:

    $ node tracker.js
      [13:37:00] Loaded configuration
      [13:37:00] Started server on :4001

Then test the server by navigating to ``data:text/html,<iframe src=http://localhost:4001>`` in your browser.

![Browser Screenshot](http://static.kaytor.ca/screenshots/noscript-tracker/browser-demo.png "Demonstration of requests sent from the browser")

![Server Screenshot](http://static.kaytor.ca/screenshots/noscript-tracker/server-demo.png "Demonstration of server output")

-----
### Embedding the tracker in a website
    
    <iframe src="http://tracking-server-example.com/" style="display:none">

-----
### Configuration

The configuration file (``config.js``) has the following configuration options:

---
``port``
Port to listen for HTTP connections
Default is ``4001``


---
``networkLeeway``
The amount of time, in seconds, to account for network latency.
Default is ``0.5``


---
``delayFunction``

Function to determine how long until the next ping is required.

The accuracy of the tracker is determined by this function.

    function(seconds) {
        
        if (seconds < 1)
            return 1;
        if (seconds < 8) 
            return Math.ceil(seconds * config.accuracy);
        else 
            return Math.floor(seconds * config.accuracy);
        
    }

